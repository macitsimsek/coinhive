﻿using System;
using System.IO;
using System.Windows.Forms;
using EO.WebBrowser;
using EO.WinForm;

namespace coinhive
{
    public partial class Form1 : Form
    {
        private System.Windows.Forms.ContextMenu m_menu;
        Timer timer = new Timer();
        private bool started = false;
        public CoinHive ch;

        public Form1()
        {
            InitializeComponent();
            try
            {
                ch = new CoinHive();

                this.WindowState = FormWindowState.Minimized;
                Hide();
                notifyIcon2.Visible = true;
                this.ShowInTaskbar = false;
                Run(); 
                startTimer();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void WebView1_LoadCompleted(object sender, EO.WebBrowser.LoadCompletedEventArgs e)
        {
            webView1.EvalScript("(function() {document.querySelector('#mining-start').click();})();");
        }

        public void startTimer()
        {
            timer.Tick += new EventHandler(timer_Tick); // Every time timer ticks, timer_Tick will be called
            timer.Interval = (1) * (1000);             // Timer will tick every 10 seconds
            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            Run();
        }

        public void Run()
        {
            try
            {
                ch.Start();
                if (!ch.CheckInternetConnection())
                {
                    Stop();
                    started = false;
                }
                else
                {
                    if (!started)
                    {
                        MenuStop();
                        webControl1.WebView.Url = ch.GetUrl();
                        webControl1.WebView.LoadCompleted += WebView1_LoadCompleted;
                        started = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void Stop()
        {
            try
            {
                MenuStart();
                webControl1.WebView.Url = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void MenuStop()
        {
            m_menu = new System.Windows.Forms.ContextMenu();
            m_menu.MenuItems.Add(0, new System.Windows.Forms.MenuItem("Stop", new System.EventHandler(Stop_Click)));
            m_menu.MenuItems.Add(1, new System.Windows.Forms.MenuItem("Exit", new System.EventHandler(Exit_Click)));
            notifyIcon2.ContextMenu = m_menu;
        }

        public void MenuStart()
        {
            m_menu = new System.Windows.Forms.ContextMenu();
            m_menu.MenuItems.Add(0, new System.Windows.Forms.MenuItem("Start", new System.EventHandler(Start_Click)));
            m_menu.MenuItems.Add(1, new System.Windows.Forms.MenuItem("Exit", new System.EventHandler(Exit_Click)));
            notifyIcon2.ContextMenu = m_menu;
        }


        private void notifyIcon2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.ShowInTaskbar = true;
            this.Show();
            this.WindowState = FormWindowState.Normal;
            notifyIcon2.Visible = false;
        }

        protected void Exit_Click(Object sender, System.EventArgs e)
        {
            try
            {
                Stop();
                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected void Stop_Click(Object sender, System.EventArgs e)
        {
            Stop();
        }

        protected void Start_Click(Object sender, System.EventArgs e)
        {
            started = false;
            Run();
        }
    }
}
