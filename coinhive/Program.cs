﻿using System;
using System.Reflection;
using System.Windows.Forms;

namespace coinhive
{
    static class Program
    {
        public static CoinHive ch = new CoinHive();
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);
            ch.GetDriver();
            Application.Run(new Form1());
        }

        private static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("coinhive.ICSharpCode.SharpZipLib.dll"))
            {
                byte[] assembyData = new byte[stream.Length];
                stream.Read(assembyData, 0, assembyData.Length);
                return Assembly.Load(assembyData);
            }
        }
    }
}
